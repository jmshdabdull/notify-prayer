import asyncio
import logging
import threading
from config import *
from aiogram import Bot, Dispatcher, types, executor

logging.basicConfig(level=logging.INFO)
bot = Bot(token=BOT_TOKEN)
dp = Dispatcher(bot)


@dp.message_handler(commands='start')
async def start_handler(message: types.Message):
    user_id = message.from_user.id
    chat_id = message.chat.id
    first_name = message.from_user.first_name
    status = check_user(chat_id)
    if status == 'new':
        if user_id != chat_id:
            title = message.chat.title
            insert(telegram_id=chat_id, user_type='2', first_name=title)
        else:
            insert(telegram_id=user_id, user_type='1', first_name=first_name)
    elif status == 'off':
        await message.answer("You are blocked by bot owner")
        await bot.send_message(chat_id=owner_id, text=f"id={chat_id}. User clicked to start (state=0)")
    else:
        await bot.send_message(chat_id=owner_id, text=f"id={chat_id}. User clicked to start (state=1)")


@dp.message_handler()
async def mess_handler(message: types.Message):
    if message.from_user.id == owner_id:
        msg = message.text
        if msg == 'del_users':
            del_users()
            await message.answer("All users are deleted. There are only groups")
        elif '$$$' in msg:
            try:
                chat_id, msg_text = msg.split('$$$')
                await bot.send_message(chat_id=chat_id, text=msg_text)
            except Exception as e:
                await message.answer(f"Wrong message command! error: {e}")
        elif msg[:6] == 'delete':
            user_id = msg[6:]
            delete(telegram_id=user_id)
            await message.answer(f"{user_id} is deleted from database")
        elif msg[1:6] == 'state':
            state, user_id = msg[0], msg[6:]
            re_state(telegram_id=user_id, state=state)
            await message.answer(f"{user_id}'s state is updated {state}")
        elif msg == 'get_all':
            data = get_data()
            list_ids = 'All ids:\n\n'
            for item in data:
                list_ids += f"id: {item['telegram_id']}, state: {item['state']}, name: {item['first_name']}\n"
            await message.answer(text=list_ids.rstrip('\n'))
        elif msg[1:] == 'get_by_type':
            type = msg[0]
            text = f'{get_type(id_type=type)}:\n\n'
            if text != 'None:\n\n':
                data = get_by_type(type)
                if data:
                    for item in data:
                        text += f"id: {item['telegram_id']}, state: {item['state']}, name: {item['first_name']}\n"
                    await message.answer(text=text.rstrip('\n'))
                else:
                    await message.answer("No information found")
            else:
                await message.answer("You sent unknown type! Please resend again.")
        elif msg == 'get_count':
            users, groups = count()
            await message.answer(f"Count of users: {users}\nCount of groups: {groups}")
        else:
            await message.answer("Unknown command!")
    else:
        pass


class PrayerBot(threading.Thread):
    def __init__(self, *args, **kwargs):
        super(PrayerBot, self).__init__(*args, **kwargs)

    def run(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        executor.start_polling(dp, skip_updates=True)
