# import schedule
# import time
# import asyncio
# from aiogram import Bot
# from aiogram.types.input_file import InputFile
# from config import *
#
#
# bot = Bot(token=BOT_TOKEN)
#
# async def send_sticker(sticker, delta):
#     if sticker: await bot.send_sticker(chat_id=owner_id, sticker=InputFile(path_or_bytesio="stickers/Ctrl_F5.webp"))
#     else: print('Sticker is None')
#     if delta:
#         time.sleep(delta)
#         await bot.send_sticker(chat_id=owner_id, sticker=InputFile(path_or_bytesio=f"stickers/{sticker}"))
#
#
# def send_mess():
#     print("Clicked /start... Bot is running")
#     asyncio.run(bot.send_message(chat_id=owner_id, text="Now bot is running!!!"))
#
#
# def run_bot(type):
#     if type == 2:
#         if datetime.today().isoweekday() != 5: sticker, delta = 'peshin.webp', 1200
#         else: sticker, delta = None, None
#     elif type == 3: sticker, delta = 'asr.webp', 600
#     elif type == 4: sticker, delta = 'shom.webp', 300
#     elif type == 5: sticker, delta = 'xufton.webp', 300
#     elif type == 6: sticker, delta = 'Juma.webp', 300
#     else: sticker, delta = None, None
#     asyncio.run(send_sticker(sticker, delta))
#
#
# def get_times():
#     r = requests.get(PRAYER_API)
#     data = r.json()
#     times = data['times']
#     return times
#
#
# def re_schedule():
#     times = get_times()
#     schedule.clear()
#     time.sleep(60)
#     schedule.every().hour.at(':00').do(send_mess)
#     schedule.every().day.at('08:30').do(re_schedule)
#     schedule.every().friday.at('09:00').do(lambda type=6: run_bot(type))
#     schedule.every().day.at('13:00').do(lambda type=2: run_bot(type))
#     schedule.every().day.at(times['asr']).do(lambda type=3: run_bot(type))
#     schedule.every().day.at(times['shom_iftor']).do(lambda type=4: run_bot(type))
#     schedule.every().day.at(times['hufton']).do(lambda type=5: run_bot(type))
#
#
# if __name__ == '__main__':
#     re_schedule()
#     while True:
#         schedule.run_pending()
#         time.sleep(1)



# def c_now():
#     now = datetime.now().time()
#     return now.hour, now.minute


# def calculate(now_hour, now_minute, prayer_hour, prayer_minute):
#     return (prayer_hour - now_hour) * 3600 + (prayer_minute - now_minute) * 60


# def get_sticker():
#     r = requests.get(PRAYER_API)
#     data = r.json()
#     today = data['weekday']
#     times = data['times']
#     asr_h, asr_m = int(times['asr'][:2]), int(times['asr'][3:])
#     shom_h, shom_m = int(times['shom_iftor'][:2]), int(times['shom_iftor'][3:])
#     xufton_h, xufton_m = int(times['hufton'][:2]), int(times['hufton'][3:])
#     # test_h, test_m = 11, 57
#
#     hour, minute = c_now()
#     if today == 'Juma' and hour <= 9 and minute < 5: return 9, 5, 'Juma.webp', None
#     # elif hour < test_h or (hour == test_h and minute < test_m): return test_h, test_m, 'Juma.webp', 60
#     elif today != 'Juma' and hour < 13: return 13, 0, 'peshin.webp', 1200
#     elif hour < asr_h or (hour == asr_h and minute < asr_m): return asr_h, asr_m, 'asr.webp', 600
#     elif hour < shom_h or (hour == shom_h and minute < shom_m): return shom_h, shom_m, 'shom.webp', 300
#     elif hour < xufton_h or (hour == xufton_h and minute < xufton_m): return xufton_h, xufton_m, 'xufton.webp', 300
#     else: return 0, 0, None, None