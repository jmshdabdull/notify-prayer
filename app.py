from bot import PrayerBot
from remind import PrayerReminder

if __name__ == '__main__':
    bot = PrayerBot()
    reminder = PrayerReminder()

    bot.start()
    reminder.start()

    bot.join()
    reminder.join()
