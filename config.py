# gitlab
BOT_TOKEN = "5761326348:AAHvgH3LXJC-0wJLW0EbAw3rhnxe2h4NWp4"
PRAYER_API = "https://islomapi.uz/api/present/day?region=Toshkent"
REALSOFT = -468956604
owner_id = 1456374097

# TEST_ID = -746336910
# TEST_TOKEN = "5400501507:AAHUak4RR9b8VdIrkEqenib5dH5tLvhNITg"


def get_data() -> list:
    data = []
    with open(file='users.txt') as file:
        rows = file.readlines()
        if rows:
            for row in rows:
                row_list = list(row.rstrip('\n').split('&$#@'))
                row_json = {
                    'telegram_id': row_list[0],
                    'state': row_list[1],
                    'type': row_list[2],
                    'first_name': row_list[3]
                }
                data.append(row_json)
    return data


def set_data(data: list) -> None:
    with open('users.txt', 'w') as file:
        for item in data:
            file.write(f"{item['telegram_id']}&$#@{item['state']}&$#@{item['type']}&$#@{item['first_name']}\n")


def check_user(telegram_id: str) -> str:
    data = get_data()
    for item in data:
        if item['telegram_id'] == str(telegram_id):
            return 'on' if item['state'] == '1' else 'off'
    return 'new'


def insert(telegram_id: str, user_type: str, first_name: str) -> None:
    with open(file='users.txt', mode='a') as file:
        try:
            file.write(f'{telegram_id}&$#@1&$#@{user_type}&$#@{first_name}\n')
        except UnicodeEncodeError:
            file.write(f'{telegram_id}&$#@1&$#@{user_type}&$#@None\n')


def delete(telegram_id: str) -> None:
    data = get_data()
    for item in data:
        if item['telegram_id'] == telegram_id:
            data.remove(item)
    set_data(data)


def del_users() -> None:
    data = get_data()
    for item in data:
        if item['type'] == '1':
            data.remove(item)
    set_data(data)


def get_by_type(type: str) -> list:
    data = get_data()
    part_data = []
    for item in data:
        if item['type'] == type:
            part_data.append(item)
    return part_data


def count() -> tuple:
    data = get_data()
    all, users = len(data), 0
    for item in data:
        if item['type'] == '1':
            users += 1
    return users, all - users - 1


def re_state(telegram_id: str, state: str) -> None:
    data = get_data()
    for item in data:
        if item['telegram_id'] == telegram_id:
            item['state'] = state
    set_data(data)


def get_type(id_type: str) -> str:
    if id_type == '1':
        return 'Users'
    elif id_type == '2':
        return 'Groups'
    elif id_type == '3':
        return 'Admin'
    else:
        return 'None'

# API so'rovlari:
#
# Bugungi namoz vatlarini olish uchun: https://islomapi.uz/api/present/day?region=Toshkent
#
# Shu hafta uchun namoz taqvimi olish uchun: https://islomapi.uz/api/present/week?region=Toshkent
#
# Bir kun uchun namoz taqvimini olish uchun: https://islomapi.uz/api/daily?region=Toshkent&month=4=4&day=5
#
# Bir oylik namoz taqvimini olish uchun: https://islomapi.uz/api/monthly?region=Toshkent&month=4
