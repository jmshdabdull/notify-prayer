# import sqlite3
# import threading
#
#
# class User(threading.Thread):
#     def __init__(self, db, *args, **kwargs) -> None:
#         super(User, self).__init__(*args, **kwargs)
#         self.conn = sqlite3.connect(db)
#         self.cur = self.conn.cursor()
#         self.cur.execute("""
#             create table if not exists users(
#                 telegram_id integer primary key,
#                 state integer default 1,
#                 type integer default 0,
#                 first_name text default ''
#             )""")
#         self.conn.commit()
#
#     def insert(self, telegram_id: int, type: int, first_name: str) -> None:
#         self.cur.execute("""
#             insert into users values (?, ?, ?, ?) on conflict (telegram_id)
#             do update set telegram_id=excluded.telegram_id,
#                           state=excluded.state,
#                           type=excluded.type,
#                           first_name=excluded.first_name
#         """, (telegram_id, 1, type, first_name))
#         self.conn.commit()
#
#     def delete(self, telegram_id: int) -> None:
#         self.cur.execute("delete from users where telegram_id=?", (telegram_id,))
#         self.conn.commit()
#
#     def re_state(self, telegram_id: int, state: int) -> None:
#         self.cur.execute("update users set state=? where telegram_id=?", (state, telegram_id))
#         self.conn.commit()
#
#     def del_users(self) -> None:
#         self.cur.execute("delete from users where type=1")
#         self.conn.commit()
#
#     def get_by_type(self, type: int):
#         self.cur.execute("select telegram_id, first_name from users where type=?", (type,))
#         return self.cur.fetchall()
#
#     @property
#     def get_all(self) -> list:
#         self.cur.execute("select telegram_id, first_name from users where state=1")
#         return self.cur.fetchall()
#
#     @property
#     def count(self):
#         self.cur.execute("select count(*) from users where state<>0")
#         all = self.cur.fetchone()[0]
#         self.cur.execute("select count(*) from users where state=1 and type=1")
#         users = self.cur.fetchone()[0]
#         return all, users
#
#     def __del__(self):
#         self.conn.close()