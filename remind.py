import time
import asyncio
import requests
import schedule
import threading
from config import *
from aiogram import Bot
from datetime import datetime
from aiogram.types.input_file import InputFile
from aiogram.utils.exceptions import Unauthorized

bot = Bot(token=BOT_TOKEN)


async def send_sticker(sticker, delta):
    data = get_data()
    for item in data:
        if item['state'] == '1' and item['type'] != '3':
            if sticker and sticker != 'Juma.webp':
                try:
                    await bot.send_sticker(
                        chat_id=item['telegram_id'],
                        sticker=InputFile(path_or_bytesio="stickers/Ctrl_F5.webp")
                    )
                except Unauthorized:
                    await bot.send_message(chat_id=owner_id, text=f"{item['telegram_id']} is not found")
            else:
                print('Sticker is None or today is Juma')
    if delta:
        time.sleep(delta)
        for item in data:
            if item['state'] == '1' and item['type'] != '3':
                try:
                    await bot.send_sticker(
                        chat_id=item['telegram_id'],
                        sticker=InputFile(path_or_bytesio=f"stickers/{sticker}")
                    )
                except Unauthorized:
                    await bot.send_message(chat_id=owner_id, text=f"{item['telegram_id']} is not found")


def send_mess():
    print("Clicked /start... Bot is running")
    asyncio.run(bot.send_message(chat_id=owner_id, text="Now bot is running!!!"))


def run_bot(prayer):
    if prayer == 2:
        if datetime.today().isoweekday() != 5:
            sticker, delta = 'peshin.webp', 900
        else:
            sticker, delta = None, None
    elif prayer == 3:
        sticker, delta = 'asr.webp', 600
    elif prayer == 4:
        sticker, delta = 'shom.webp', 300
    elif prayer == 5:
        sticker, delta = 'xufton.webp', 300
    elif prayer == 6:
        sticker, delta = 'Juma.webp', 300
    else:
        sticker, delta = None, None
    asyncio.run(send_sticker(sticker, delta))


def get_times():
    r = requests.get(PRAYER_API)
    data = r.json()
    times = data['times']
    return times


def re_schedule():
    times = get_times()
    schedule.clear()
    time.sleep(60)
    schedule.every().day.at('12:00').do(send_mess)
    schedule.every().day.at('08:30').do(re_schedule)
    schedule.every().friday.at('09:00').do(lambda prayer=6: run_bot(prayer))
    schedule.every().day.at('12:45').do(lambda prayer=2: run_bot(prayer))
    schedule.every().day.at(times['asr']).do(lambda prayer=3: run_bot(prayer))
    schedule.every().day.at(times['shom_iftor']).do(lambda prayer=4: run_bot(prayer))
    schedule.every().day.at(times['hufton']).do(lambda prayer=5: run_bot(prayer))


class PrayerReminder(threading.Thread):
    def __init__(self, *args, **kwargs):
        super(PrayerReminder, self).__init__(*args, **kwargs)

    def run(self):
        re_schedule()
        while True:
            schedule.run_pending()
            time.sleep(1)
